Here are some of my own cogs and also some modified cogs of other users.

The modified cogs are either corrected or extended. It depends on the cog itself.


Installation:

Replace `[p]` with your bot's prefix.
```
[p]cog repo add dimxxz-Cogs https://gitlab.com/dimxxz/dimxxz-Cogs
```
```
[p]cog install dimxxz-Cogs <cog name here>
```
