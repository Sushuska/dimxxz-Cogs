Raidprotect v2.2

Original cog written by PlanetTeamSpeakk/PTSCogs https://github.com/PlanetTeamSpeakk/PTSCogs

Rewritten, modified and improved by me, dimxxz

Commands:

```
[p]raidprotect				- Group command
[p]raidprotect members	    	- Sets the amount of members that will trigger raid-protect. 
[p]raidprotect toggle			- Toggle raid-protect v2.2
[p]raidprotect active			- Activates raid-protect v2.2
[p]raidprotect setup			- Setup the channel and role for raid-protect v2.2
```


Installation:

Replace `[p]` with your bot's prefix.
```
[p]cog repo add dimxxz-Cogs https://gitlab.com/dimxxz/dimxxz-Cogs
```
```
[p]cog install dimxxz-Cogs raidprotect
```
