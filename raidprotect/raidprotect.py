import discord
from discord.ext import commands
from .utils import checks
from cogs.utils.dataIO import dataIO, fileIO
import os
import asyncio
from __main__ import settings
from __main__ import send_cmd_help
import logging
from copy import deepcopy

__author__ = "dimxxz - https://github.com/dimxxz/dimxxz-Cogs"
__version__ = '2.2.1'

log = logging.getLogger("red.admin")

class Raid:

    def __init__(self, bot, raid):
        self.xraid = raid
        self.bot = bot

    def check_raid(self, server):
        try:
            if self.xraid[server.id]['protected']:
                return True
            elif not self.xraid[server.id]['protected']:
                return False
        except:
            return False

class RaidProtect:
    """Protect yourself from server raids."""

    def __init__(self, bot):
        self.bot = bot
        self.settings = dataIO.load_json("data/raidprotect/settings.json")
        self.raid_check = Raid(bot, self.settings)

    def _role_from_string(self, server, rolename, roles=None):
        if roles is None:
            roles = server.roles

        roles = [r for r in roles if r is not None]
        role = discord.utils.find(lambda r: r.name.lower() == rolename.lower(),
                                  roles)
        try:
            log.debug("Role {} found from rolename {}".format(
                role.name, rolename))
        except Exception:
            log.debug("Role not found for rolename {}".format(rolename))
        return role

    async def _auto_give(self, member, roleid):
        role = discord.utils.get(member.server.roles, id=roleid)
        await self.bot.add_roles(member, role)

    @commands.group(pass_context=True)
    async def raidprotect(self, ctx):
        """Manage Raid-Protect v2.2.1"""
        server = ctx.message.server
        if server.id not in self.settings:
            self.settings[server.id] = {'joined': 0, 'members': 3, 'protected': False, 'active': False,
                                        'setup': False}
            self.save_settings()
            await asyncio.sleep(1)
        if 'active' not in self.settings[server.id]:
            self.settings[server.id]['active'] = False
            self.settings[server.id]['setup'] = False
            self.save_settings()
            await asyncio.sleep(1)

        if ctx.invoked_subcommand is None:
            await send_cmd_help(ctx)

            await asyncio.sleep(1)

            if self.settings[server.id]['setup'] == False:
                return
            title = '**Anti-Raid System Settings:**\n'
            msg = 'Active: *{}*\n'.format("Yes" if self.settings[server.id]['active'] == True else "No")
            msg += 'Running: *{}*\n'.format("Yes" if self.settings[server.id]['protected'] == True else "No")
            msg += 'Members: **{}**\n'.format(self.settings[server.id]['members'])
            e = discord.Embed(title=title, description=msg, color=discord.Color.blue())
            await self.bot.say(embed=e)
            
    @raidprotect.command(pass_context=True)
    @checks.mod_or_permissions(administrator=True)
    async def setup(self, ctx):
        """Setup the channel and role for raid-protect v2.2.1"""
        chrname = 'raid'
        server = ctx.message.server
        if self.settings[server.id]['setup'] == True:
            msg = ("Raid-protect has already been set up on your server.\nPlease use the commands from the list if you need to change something.")
            em = discord.Embed(title="Anti-Raid v2.2", description=msg, colour=server.me.color)
            await self.bot.say(embed=em)
            return
        
        msg = ("Raid-Protect will secure your server when a bunch of users"
               " start joining and spamming your server with trash (*aka server raid*).\n\nI will guide you through"
               " the setup of this feature!\nYou can quit any time by replying **quit**")
        em = discord.Embed(title="Welcome to Raid-Protect v2.2", description=msg, colour=server.me.color)
        await self.bot.say(embed=em)
        await asyncio.sleep(15)

        msg1 = ("1. First of all we need to set up your server's admin / mod role.\nThis is required for the raid protection to work!")
        em1 = discord.Embed(description=msg1, colour=server.me.color)
        await self.bot.say(embed=em1)
        await asyncio.sleep(7)

        msg1 = ("Please write your server's **admin role** after this message. Don't use mentions!\n"
                "If you mess this part up, you can still use **{}set adminrole** to fix it. (30s timeout)".format(ctx.prefix))
        em1 = discord.Embed(description=msg1, colour=server.me.color)
        await self.bot.say(embed=em1)
        answer = await self.bot.wait_for_message(timeout=30, author=ctx.message.author)
        if answer.content.lower().startswith('quit'):
            msg1 = ("Alright, quitting the setup process!")
            em1 = discord.Embed(description=msg1, colour=discord.Colour.orange())
            await self.bot.say(embed=em1)
            return
        elif answer != None:
            new_msg = deepcopy(ctx.message)
            new_msg.author = ctx.message.author
            new_msg.content = self.bot.settings.get_prefixes(new_msg.server)[0] + "set adminrole {}".format(answer.content)
            await self.bot.process_commands(new_msg)
        elif answer is None:
            msg1 = ("Admin role not set!")
            em1 = discord.Embed(description=msg1, colour=discord.Colour.orange())
            await self.bot.say(embed=em1)
        else:
            msg1 = ("Admin role not set!")
            em1 = discord.Embed(description=msg1, colour=discord.Colour.orange())
            await self.bot.say(embed=em1)
        await asyncio.sleep(7)

        msg1 = ("Please write your server's **mod role** after this message. Don't use mentions!\n"
                "If you mess this part up, you can still use **{}set modrole** to fix it. (30s timeout)".format(ctx.prefix))
        em1 = discord.Embed(description=msg1, colour=server.me.color)
        await self.bot.say(embed=em1)
        answer = await self.bot.wait_for_message(timeout=30, author=ctx.message.author)
        if answer.content.lower().startswith('quit'):
            msg1 = ("Alright, quitting the setup process!")
            em1 = discord.Embed(description=msg1, colour=discord.Colour.orange())
            await self.bot.say(embed=em1)
            return
        elif answer != None:
            new_msg = deepcopy(ctx.message)
            new_msg.author = ctx.message.author
            new_msg.content = self.bot.settings.get_prefixes(new_msg.server)[0] + "set modrole {}".format(answer.content)
            await self.bot.process_commands(new_msg)
        elif answer is None:
            msg1 = ("Mod role not set!")
            em1 = discord.Embed(description=msg1, colour=discord.Colour.orange())
            await  self.bot.say(embed=em1)
        else:
            msg1 = ("Mod role not set!")
            em1 = discord.Embed(description=msg1, colour=discord.Colour.orange())
            await  self.bot.say(embed=em1)
        await asyncio.sleep(7)

        msg2 = ("2. Would you like to activate Raid-Protect now? (yes/no) (30s timeout)")
        em2 = discord.Embed(description=msg2, colour=server.me.color)
        await self.bot.say(embed=em2)
        answer = await self.bot.wait_for_message(timeout=30, author=ctx.message.author)
        if answer.content.lower().startswith('quit'):
            msg1 = ("Alright, quitting the setup process!")
            em1 = discord.Embed(description=msg1, colour=discord.Colour.orange())
            await self.bot.say(embed=em1)
            return
        elif answer.content.lower().startswith('yes'):
            self.settings[server.id]['active'] = True
            msg1 = ("Raid-protect activated!")
            em1 = discord.Embed(description=msg1, colour=discord.Colour.green())
            await self.bot.say(embed=em1)
        elif answer.content.lower().startswith('no'):
            self.settings[server.id]['active'] = False
            msg1 = ("Raid-protect deactivated!")
            em1 = discord.Embed(description=msg1, colour=discord.Colour.orange())
            await self.bot.say(embed=em1)
        elif answer is None:
            self.settings[server.id]['active'] = False
            msg1 = ("Raid-protect deactivated!")
            em1 = discord.Embed(description=msg1, colour=discord.Colour.orange())
            await self.bot.say(embed=em1)
        else:
            self.settings[server.id]['active'] = False
            msg1 = ("Raid-protect deactivated!")
            em1 = discord.Embed(description=msg1, colour=discord.Colour.orange())
            await self.bot.say(embed=em1)
        self.save_settings()

        await asyncio.sleep(7)

        msg3 = ("3. Please write the number of users that will trigger Raid-Protect on member-join events.\n\n"
                "Recommended: 3 or 4  (30s timeout)")
        em3 = discord.Embed(description=msg3, colour=server.me.color)
        await self.bot.say(embed=em3)
        answer = await self.bot.wait_for_message(timeout=30, author=ctx.message.author)
        if answer.content.lower().startswith('quit'):
            msg1 = ("Alright, quitting the setup process!")
            em1 = discord.Embed(description=msg1, colour=discord.Colour.orange())
            await self.bot.say(embed=em1)
            return
        elif answer != None:
            new_msg = deepcopy(ctx.message)
            new_msg.author = ctx.message.author
            new_msg.content = self.bot.settings.get_prefixes(new_msg.server)[0] + "raidprotect members {}".format(answer.content)
            await self.bot.process_commands(new_msg)
        elif answer is None:
            msg1 = ("Amount of users not set!")
            em1 = discord.Embed(description=msg1, colour=discord.Colour.orange())
            await self.bot.say(embed=em1)
        else:
            msg1 = ("Amount of users not set!")
            em1 = discord.Embed(description=msg1, colour=discord.Colour.orange())
            await self.bot.say(embed=em1)

        await asyncio.sleep(7)

        msg4 = "4. Setting up Anti-Raid!"
        em4 = discord.Embed(description=msg4, colour=discord.Colour.orange())
        await self.bot.say(embed=em4)
        await asyncio.sleep(2)

        rolev = self._role_from_string(server, chrname)
        if not rolev:
            everyone_perms = discord.PermissionOverwrite(read_messages=False)
            rolev = await self.bot.create_role(server, name=chrname)
            perms = discord.PermissionOverwrite()
            ch = discord.utils.get(server.channels, name=chrname)
            if not ch:
                admin_role = discord.utils.get(server.roles, name=settings.get_server_admin(server))
                mod_role = discord.utils.get(server.roles, name=settings.get_server_mod(server))
                everyone_perms = discord.PermissionOverwrite(read_messages=False)
                insilenced_perms = discord.PermissionOverwrite(read_messages=True, send_messages=True)
                mod_admin_perms = discord.PermissionOverwrite(read_messages=True, send_messages=True,
                                                              manage_channel=True)
                chn = await self.bot.create_channel(
                    server, chrname,
                    (server.default_role, everyone_perms),
                    (rolev, insilenced_perms),
                    (admin_role, mod_admin_perms),
                    (mod_role, mod_admin_perms))
                e = discord.Embed(title="Anti-Raid", description="Raid-Protection has been set up!",
                                  colour=discord.Colour.red())
                await self.bot.send_message(chn, embed=e)
                await asyncio.sleep(1)
                for c in server.channels:
                    if c.name != chn.name:
                        try:
                            await self.bot.edit_channel_permissions(c, rolev, everyone_perms)
                        except discord.errors.Forbidden:
                            pass
                e2 = discord.Embed(description="Raid-Protection is now active! Contact a Staff Member for"
                                               " help or wait until you get verified!",
                                   colour=discord.Colour.red())
                await self.bot.send_message(chn, embed=e2)

            ch = discord.utils.get(server.channels, name=chrname)
            if ch.type == discord.ChannelType.text:
                perms.send_messages = True
                perms.read_messages = True
            await self.bot.edit_channel_permissions(ch, rolev, overwrite=perms)
            for c in server.channels:
                if c.name != ch.name:
                    try:
                        await self.bot.edit_channel_permissions(c, rolev, everyone_perms)
                    except discord.errors.Forbidden:
                        pass

            msgx = "Raid Role created! Proceeding with Channel creation..."
            emx = discord.Embed(description=msgx, colour=discord.Colour.green())
            await self.bot.say(embed=emx)
        else:
            msgx = "Raid Role already exists! Proceeding with Channel creation..."
            emx = discord.Embed(description=msgx, colour=discord.Colour.green())
            await self.bot.say(embed=emx)
        await asyncio.sleep(2)
        ch = discord.utils.get(server.channels, name=chrname)
        if not ch:
            rolev = self._role_from_string(server, chrname)
            admin_role = discord.utils.get(server.roles, name=settings.get_server_admin(server))
            mod_role = discord.utils.get(server.roles, name=settings.get_server_mod(server))
            everyone_perms = discord.PermissionOverwrite(read_messages=False)
            insilenced_perms = discord.PermissionOverwrite(read_messages=True, send_messages=True)
            mod_admin_perms = discord.PermissionOverwrite(read_messages=True, send_messages=True,
                                                          manage_channel=True)
            chn = await self.bot.create_channel(
                server, chrname,
                (server.default_role, everyone_perms),
                (rolev, insilenced_perms),
                (admin_role, mod_admin_perms),
                (mod_role, mod_admin_perms))
            e = discord.Embed(title="Anti-Raid", description="Raid-Protection has been set up!",
                              colour=discord.Colour.red())
            await self.bot.send_message(chn, embed=e)
            await asyncio.sleep(1)
            for c in server.channels:
                if c.name != chn.name:
                    try:
                        await self.bot.edit_channel_permissions(c, rolev, everyone_perms)
                    except discord.errors.Forbidden:
                        pass
            e2 = discord.Embed(description="Raid-Protection is now active! Contact a Staff Member for"
                                           " help or wait until you get verified!",
                               colour=discord.Colour.red())
            await self.bot.send_message(chn, embed=e2)
            msgx = "Raid Channel created!"
            emx = discord.Embed(description=msgx, colour=discord.Colour.green())
            await self.bot.say(embed=emx)
        else:
            msgx1 = "Raid Channel already exists!"
            emx1 = discord.Embed(description=msgx1, colour=discord.Colour.green())
            await self.bot.say(embed=emx1)

        await asyncio.sleep(5)

        msg7 = ("The setup is now complete!\nIf you want to change the settings, please use the commands from the list.")
        em7 = discord.Embed(title="Setup Complete", description=msg7, colour=discord.Colour.green())
        await self.bot.say(embed=em7)
        self.settings[server.id]['setup'] = True
        self.save_settings()

    @raidprotect.command(name="fix", pass_context=True)
    @checks.admin_or_permissions(manage_server=True)
    async def r_fix(self, ctx):
        """Fix Raid-Protect v2.2.1"""
        server = ctx.message.server
        msg = "Fixing Raid-Protect Settings...."
        em = discord.Embed(description=msg, colour=server.me.color)
        await self.bot.say(embed=em)

        chrname = 'raid'
        rolev = self._role_from_string(server, chrname)
        if not rolev:
            everyone_perms = discord.PermissionOverwrite(read_messages=False)
            rolev = await self.bot.create_role(server, name=chrname)
            perms = discord.PermissionOverwrite()
            ch = discord.utils.get(server.channels, name=chrname)
            if ch.type == discord.ChannelType.text:
                perms.send_messages = True
                perms.read_messages = True
            await self.bot.edit_channel_permissions(ch, rolev, overwrite=perms)
            for c in server.channels:
                if c.name != ch.name:
                    try:
                        await self.bot.edit_channel_permissions(c, rolev, everyone_perms)
                    except discord.errors.Forbidden:
                        pass

            msgx = "Raid Role fixed! Proceeding with Channel fix..."
            emx = discord.Embed(description=msgx, colour=discord.Colour.green())
            await self.bot.say(embed=emx)
        else:
            msgx = "Raid Role already exists! Proceeding with Channel fix..."
            emx = discord.Embed(description=msgx, colour=discord.Colour.green())
            await self.bot.say(embed=emx)
        ch = discord.utils.get(server.channels, name=chrname)
        if not ch:
            rolev = self._role_from_string(server, chrname)
            admin_role = discord.utils.get(server.roles, name=settings.get_server_admin(server))
            mod_role = discord.utils.get(server.roles, name=settings.get_server_mod(server))
            everyone_perms = discord.PermissionOverwrite(read_messages=False)
            insilenced_perms = discord.PermissionOverwrite(read_messages=True, send_messages=True)
            mod_admin_perms = discord.PermissionOverwrite(read_messages=True, send_messages=True,
                                                          manage_channel=True)
            chn = await self.bot.create_channel(
                server, chrname,
                (server.default_role, everyone_perms),
                (rolev, insilenced_perms),
                (admin_role, mod_admin_perms),
                (mod_role, mod_admin_perms))
            e = discord.Embed(title="Anti-Raid", description="Raid-Protection has been set up!",
                              colour=discord.Colour.red())
            await self.bot.send_message(chn, embed=e)

            await asyncio.sleep(1)

            for c in server.channels:
                if c.name != chn.name:
                    try:
                        await self.bot.edit_channel_permissions(c, rolev, everyone_perms)
                    except discord.errors.Forbidden:
                        pass
            e2 = discord.Embed(description="Raid-Protection is now active! Contact a Staff Member for"
                                           " help or wait until you get verified!",
                               colour=discord.Colour.red())
            await self.bot.send_message(chn, embed=e2)
            msgx = "Raid Channel fixed! Finished Raid-Protect fix!"
            emx = discord.Embed(description=msgx, colour=discord.Colour.green())
            await self.bot.say(embed=emx)
        else:
            msgx1 = "Raid Channel already exists! Finished Raid-Protect fix!"
            emx1 = discord.Embed(description=msgx1, colour=discord.Colour.green())
            await self.bot.say(embed=emx1)
            
    @raidprotect.command(name="toggle", pass_context=True)
    @checks.admin_or_permissions(ban_members=True)
    async def r_toggle(self, ctx):
        """Toggle raid-protect v2.2.1"""
        server = ctx.message.server
        if self.settings[server.id]['protected']:
            self.settings[server.id]['protected'] = False
            message = ("Raid-Protect Status: **Standby**")
            em = discord.Embed(description=message, color=discord.Color.blue())
            await self.bot.say(embed=em)
        else:
            self.settings[server.id]['protected'] = True
            message = ("Raid-Protect Status: **Active**")
            em = discord.Embed(description=message, color=discord.Color.blue())
            await self.bot.say(embed=em)
        self.save_settings()

    @raidprotect.command(name="active", pass_context=True)
    @checks.admin_or_permissions(manage_server=True)
    async def r_active(self, ctx):
        """De-/activate raid-protect v2.2.1"""
        server = ctx.message.server
        if self.settings[server.id]['active']:
            self.settings[server.id]['active'] = False
            message = ("Raid protection has been deactivated on your server!")
            em = discord.Embed(description=message, color=discord.Color.blue())
            await self.bot.say(embed=em)
        else:
            self.settings[server.id]['active'] = True
            message = ("Raid protection has been activated on your server!")
            em = discord.Embed(description=message, color=discord.Color.blue())
            await self.bot.say(embed=em)
        self.save_settings()

    @raidprotect.command(name="test", pass_context=True)
    @checks.admin_or_permissions(manage_server=True)
    async def r_test(self, ctx):
        server = ctx.message.server
        channel = "raid"
        ch = discord.utils.get(server.channels, name=channel)
        await self.bot.say("Channel Name: {}\nChannel ID: {}".format(ch.name, ch.id))
        rolename = 'raid'
        role = self._role_from_string(ctx.message.server, rolename)
        await self.bot.say("Role name: {}\nRole ID: {}".format(role.name, role.id))

    @raidprotect.command(name="members", pass_context=True)
    @checks.admin_or_permissions(manage_server=True)
    async def r_members(self, ctx, members:int):
        """Sets how many members should join within 20 seconds before the bot will protect the server.
        0 is unlimited, so that will turn it off. Default is 4."""
        self.settings[ctx.message.server.id]['members'] = members
        self.save_settings()
        title= "Raid-Protect Settings:"
        em = discord.Embed(title=title, description="Member count set to **{}**!".format(members), color=discord.Color.blue())
        await self.bot.say(embed=em)

    @raidprotect.command(name="help", pass_context=True)
    @checks.admin_or_permissions(manage_server=True)
    async def r_help(self, ctx):
        """Help for Raid-Protect v2.2.1"""
        title= "Raid-Protect 2.2.1 Help"
        msg = ("1. How do I remove users from the raid channel?\n"
               "    Remove the **raid** role from the user.\n\n"
               "2. How to stop Raid-Protect?\n"
               "    **{0}raidprotect toggle** <- This will stop the protection until it gets triggered again.\n"
               "    **{0}raidprotect active**  <- This can deactivate the protection completely.\n\n"
               "3. The raid role or channel have been deleted. What now?\n"
               "    Run **{0}raidprotect fix**\n\n"
               "4. I messed up the admin role and mod role in the setup process...\n"
               "    Run **{0}set adminrole** and **{0}set modrole** to fix this part.".format(ctx.prefix))
        em = discord.Embed(title=title, description=msg, color=discord.Color.blue())
        await self.bot.say(embed=em)


    def save_settings(self):
        dataIO.save_json("data/raidprotect/settings.json", self.settings)

        
    async def member_join(self, member):
        if (member.server.id in self.settings) and not ("bots" in member.server.name.lower()):
            #Raid-Protect
            if self.settings[member.server.id]['active']:
                try:
                    temp = self.settings[member.server.id]['joined']
                except KeyError:
                    self.settings[member.server.id]['joined'] = 0
                try:
                    self.settings[member.server.id]['joined'] += 1
                except:
                    pass

            if self.settings[member.server.id]['protected']:
                rolename = str('raid')
                role = self._role_from_string(member.server, rolename)
                await self._auto_give(member, role.id)

            if self.settings[member.server.id]['members'] != 0:
                if (self.settings[member.server.id]['joined'] >= self.settings[member.server.id]['members']) and not (
                    self.settings[member.server.id]['protected']):
                    self.settings[member.server.id]['protected'] = True
                    self.save_settings()
                    if self.settings[member.server.id]['protected']:
                        rolename = str('raid')
                        role = self._role_from_string(member.server, rolename)
                        await self._auto_give(member, role.id)

            await asyncio.sleep(20)

            self.settings[member.server.id]['joined'] = 0
            self.save_settings()

            if self.settings[member.server.id]['protected']:
                rolename = str('raid')
                role = self._role_from_string(member.server, rolename)
                await self._auto_give(member, role.id)


    async def on_channel_create(self, c, role=None):
        """Run when new channels are created and set up role permissions"""
        if not c.server.id in self.settings:
            return
        if c.is_private:
            return
        try:
            perms = discord.PermissionOverwrite()
            if c.type == discord.ChannelType.text:
                perms.send_messages = False
                perms.send_tts_messages = False
                perms.read_messages = False
            elif c.type == discord.ChannelType.voice:
                perms.speak = False
            if not role:
                role = discord.utils.get(c.server.roles, name="raid")
            if c.name == "raid":
                pass
            else:
                await self.bot.edit_channel_permissions(c, role, overwrite=perms)
        except:
            pass


def check_folders():
    if not os.path.exists("data/raidprotect"):
        print("Creating data/raidprotect folder...")
        os.makedirs("data/raidprotect")
        
def check_files():
    if not os.path.exists("data/raidprotect/settings.json"):
        print("Creating data/raidprotect/settings.json file...")
        dataIO.save_json("data/raidprotect/settings.json", {})
        
def setup(bot):
    check_folders()
    check_files()
    n = RaidProtect(bot)
    bot.add_listener(n.member_join, "on_member_join")
    bot.add_cog(n)